package edu.duke.xh110.battleship;

import java.util.HashSet;

public class RectangleShip<T> extends BasicShip<T>{
  private final String name;
  public RectangleShip(String name, Coordinate upperLeft, int width, int height, SimpleShipDisplayInfo<T> myInfo, SimpleShipDisplayInfo<T> enemyInfo, char orientation) {
    super(makeCoords(upperLeft, width, height), myInfo, enemyInfo, upperLeft, orientation == 'H' ? 0 : 1,
          height,
          width);
    this.name = name;
  }
  public RectangleShip(String name, Coordinate upperLeft, int width, int height, T mydata, T onHit, char orientataion) {
    this(name, upperLeft, width, height, new SimpleShipDisplayInfo<T>(mydata, onHit), new SimpleShipDisplayInfo<T>(null, mydata), orientataion);
  }
  public RectangleShip(Coordinate upperLeft, T data, T onHit) {
    this("testship", upperLeft, 1, 1, data, onHit, 'V');
  }
  @Override
  public String getName() {
    return name;
  }

  
  public static HashSet<Coordinate> makeCoords(Coordinate upperLeft, int width, int height) {
    /**
     * This method should generate the set of coordinates
     * for a rectangle starting at upperLeft whose width
     * and height are specified.
     */
    HashSet<Coordinate> set = new HashSet<Coordinate>();
    for (int i = 0; i < height; i++) {
      for (int j = 0; j < width; j++) {
        set.add(new Coordinate(i + upperLeft.getRow(), j + upperLeft.getColumn()));
      }
    }
    return set;
  }
}
