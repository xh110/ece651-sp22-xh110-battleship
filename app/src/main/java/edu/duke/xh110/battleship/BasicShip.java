package edu.duke.xh110.battleship;

import java.util.HashMap;

public abstract class BasicShip<T> implements Ship<T> {
  /**
  * null false true from myPieces.get(someCoordinate)
  * to indicate if a Coordinate is part of current ship
  * and if this part of ship is destroyed or not 
  */
  private HashMap<Coordinate, Boolean> myPieces;
  private Coordinate upperLeft;
  private int orientation;
  private int row;
  private int col;
  protected ShipDisplayInfo<T> myDisplayInfo;
  protected ShipDisplayInfo<T> enemyDisplayInfo;

  public BasicShip(Iterable<Coordinate> where, ShipDisplayInfo<T> myDisplayInfo, ShipDisplayInfo<T> enemyDisplayInfo, Coordinate upperLeft, int orientation, int row, int col) {
    myPieces = new HashMap<Coordinate, Boolean>();
    for (Coordinate c: where) {
      /**
       * This should initialize myPiece to have each Coordinate
       * in where mapped to false.
       */
      myPieces.put(c, false);
    }
    this.myDisplayInfo = myDisplayInfo;
    this.enemyDisplayInfo = enemyDisplayInfo;
    this.upperLeft = upperLeft;
    this.orientation = orientation;
    this.row = row;
    this.col = col;
  }

  protected void checkCoordinateInThisShip(Coordinate c) {
    if (myPieces.containsKey(c) == false)
      throw new IllegalArgumentException("This Coordinate is not in the ship!\n");
  }
  @Override
  public HashMap<Coordinate, Boolean> getPieces() {
    return myPieces;
  }
  @Override
  public void replacePieces(HashMap<Coordinate, Boolean> newPieces) {
    myPieces = newPieces;
  }

  @Override
  public boolean occupiesCoordinates(Coordinate where) {
    // TODO Auto-generated method stub
    // used to return just a location
    // now will check all the ocuupy loacation
    // and return the answer
    return myPieces.containsKey(where);
  }

  @Override
  public boolean isSunk() {
    // TODO Auto-generated method stub
    // if all the Coordinate of this Ship is
    // hitted the Ship is Sunk
    if (myPieces.containsValue(false))
      return false;
    return true;
  }

  @Override
  public void recordHitAt(Coordinate where) {
    // TODO Auto-generated method stub
    checkCoordinateInThisShip(where);
    myPieces.put(where, true);
  }

  @Override
  public boolean wasHitAt(Coordinate where) {
    // TODO Auto-generated method stub
    checkCoordinateInThisShip(where);
    return myPieces.get(where);
  }

  @Override
  public T getDisplayInfoAt(Coordinate where, boolean myShip) {
    // TODO this is not right. We need to
    // look up the hit status of this coordinate
    // where, onHit
    if (myShip)
      return myDisplayInfo.getinfo(where, wasHitAt(where));
    else
      return enemyDisplayInfo.getinfo(where, wasHitAt(where));
  }
  
  @Override
  public Iterable<Coordinate> getCoordiantes() {
    // return a Set of Keys
    return myPieces.keySet();
  }

  @Override
  public void setOrientation(int orientation) {
    this.orientation = orientation;
  }
  @Override
  public int getOrientation() {
    return orientation;
  }
  @Override
  public Coordinate getUpperLeft() {
    return upperLeft;
  }
  @Override
  public void modifyUpperLeft(Coordinate c) {
    upperLeft = c;
  }

  @Override
  public int getRow() {return row;}
  @Override
  public int getCol() {return col;}
  @Override
  public void changeRow(int row) {this.row = row;}
  @Override
  public void changeCol(int col) {this.col = col;}


}
