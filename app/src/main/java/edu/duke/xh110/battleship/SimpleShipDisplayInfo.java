package edu.duke.xh110.battleship;

public class SimpleShipDisplayInfo<T> implements ShipDisplayInfo<T> {
  private T myData;
  private T onHit;

  public SimpleShipDisplayInfo(T myData, T onHit) {
    this.myData = myData;
    this.onHit = onHit;
  }
  
  @Override
  public T getinfo(Coordinate where, boolean hit) {
    // TODO Auto-generated method stub
    return hit ? onHit : myData;
  }

}
