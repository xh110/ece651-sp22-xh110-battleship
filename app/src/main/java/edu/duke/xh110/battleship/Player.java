package edu.duke.xh110.battleship;

public interface Player {
  public void playOneTurn(Player player);
  public void doPlacementPhase();
}
