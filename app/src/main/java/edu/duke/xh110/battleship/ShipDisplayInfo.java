package edu.duke.xh110.battleship;

public interface ShipDisplayInfo<T> {
  public T getinfo(Coordinate where, boolean hit);
}
