package edu.duke.xh110.battleship;

import java.util.ArrayList;

/**
 * check if the ship can be placed on the
 * Board without having a collision with
 * other ships
 */
public class NoCollisionRuleChecker<T> extends PlacementRuleChecker<T>{
  public NoCollisionRuleChecker(PlacementRuleChecker<T> next) {
    super(next);
  }
  
  @Override
  protected String checkMyRule(Ship<T> theShip, Board<T> theBoard) {
    // check each ship that are placed on theBoard
    // make sure no one ship will have a collision
    // with the current ship
    for (Coordinate where: theShip.getCoordiantes()) {
      if (theBoard.whatIsAtForSelf(where) != null) {
        ArrayList<Ship<T>> arr = theBoard.whatShipListAt(where);
        for  (Ship<T> s: arr) {
          if (s != theShip)
            return "That placement is invalid: the ship overlaps another ship.\n";
        }
      }
    }
    return null;
  }
  
}
