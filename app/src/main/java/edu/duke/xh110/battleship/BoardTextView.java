package edu.duke.xh110.battleship;

import java.util.Collections;
import java.util.function.Function;

/**
 *This class handles textual display of
 *a Board (i.e., converting it to a string to show
 *to the user).
 *It supports two ways to display the Board:
 *one fir the player's own board, and one for the 
 *enemy's board
 */
public class BoardTextView {
  /**
   *The Board to display
   */
  private final Board<Character> toDisplay;
  /**
   *constructs a BoardView, given the board it will display.
   *@param toDisplay is the Board to disaply
   */
  public BoardTextView(Board<Character> toDisplay) {
    this.toDisplay = toDisplay;
    if (toDisplay.getWidth() > 10 || toDisplay.getHeight() > 26) {
      throw new IllegalArgumentException(
                                        "Board must be no larger than 10x26, but is"+toDisplay.getWidth()+"x"+toDisplay.getHeight());
    }
  }

  /**
   * display the whole Board
   */
  public String displayAnyBoard(Function<Coordinate, Character> getSquareFn) {
    StringBuilder ans = new StringBuilder();
    ans.append(makeHeader());
    for (int i = 0; i < toDisplay.getHeight(); i++) {
      ans.append(makeBoardLine(i, getSquareFn));
    }
    ans.append(makeHeader());
    return ans.toString();//this is a placeholder for the moments
  }

  public String displayMyOwnBoard() {
    return displayAnyBoard((c)->toDisplay.whatIsAtForSelf(c));
  }
  public String displayEnemyBoard() {
    return displayAnyBoard((c)->toDisplay.whatIsAtForEnemy(c));
  }

  public String displayMyBoardWithEnemyNextToIt(BoardTextView enemyView, String myHeader,
                                                String enemyHeader) {
    String space;
    StringBuilder sb = new StringBuilder();
    String[] my_arr = displayMyOwnBoard().split("\n");
    String[] enemy_arr = enemyView.displayEnemyBoard().split("\n");
    int len = toDisplay.getWidth() * 2 + 3 - 5 - myHeader.length();
    len = len < 0 ? 0 : len;
    space = String.join("", Collections.nCopies(19 + len, " "));
    sb.append("     " + myHeader + space + enemyHeader + "\n");
    for (int i = 0; i < my_arr.length; i++) {
      int extra = (i == 0 || i == my_arr.length - 1) ? 2 : 0;
      space = String.join("", Collections.nCopies(16 + extra, " "));
      sb.append(my_arr[i] + space + enemy_arr[i] + "\n");
    }
    return sb.toString();
  }

  /**
   * showBoardline
   */
  String makeBoardLine(int row, Function<Coordinate, Character> getSquareFn) {
    StringBuilder ans = new StringBuilder();
    ans.append((char)('A' + row));
    ans.append(' ');
    for (int col = 0; col < toDisplay.getWidth(); col++) {
      if (col != 0) {
        ans.append("|");
      }
      // whatIs
      // TODO
      Character c = getSquareFn.apply(new Coordinate(row, col));
      ans.append(c == null ? ' ' : c);
    }
    ans.append(' ');
    ans.append((char)('A' + row));
    ans.append("\n");
    return ans.toString();
  }
  /**
   * make header
   */
  String makeHeader() {
    StringBuilder ans = new StringBuilder("  "); //README shows two spaces at
    String sep = "";//start with nothing to separate, then switch to | to separate
    for (int i = 0; i < toDisplay.getWidth();i++) {
      ans.append(sep);
      ans.append(i);
      sep = "|";
    }
    ans.append("\n");
    return ans.toString();
  }
}
