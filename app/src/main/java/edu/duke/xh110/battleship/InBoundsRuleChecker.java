package edu.duke.xh110.battleship;

public class InBoundsRuleChecker<T> extends PlacementRuleChecker<T> {
  public InBoundsRuleChecker(PlacementRuleChecker<T> next) {
    super(next);
  }
  
  @Override
  protected String checkMyRule(Ship<T> theShip, Board<T> theBoard) {
    // iterate all over the coordinates in
    // ships to check that they are
    // in bounds on theBoard
    int row = theBoard.getHeight();
    int column = theBoard.getWidth();
    Iterable<Coordinate> set = theShip.getCoordiantes();
    for (Coordinate where: set) {
      int i = where.getRow();
      int j = where.getColumn();
      if (i < 0 || i >= row || j < 0 || j >= column) {
        String mes_begin = "That placement is invalid: the ship goes off the ";
        String mes_end = " of the board.\n";
        if (i < 0) {
          return mes_begin + "top" + mes_end;
        }
        else if (i >= row) {
          return mes_begin + "bottom" + mes_end;
        }
        else if (j < 0) {
          return mes_begin + "left" + mes_end;
        }
        else {
          return mes_begin + "right" + mes_end;
        }
      }
    }
    return null;
  }
  

}
