package edu.duke.xh110.battleship;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.function.Function;

public class HumanTextPlayer extends TextPlayer{
  public HumanTextPlayer(String player, Board<Character> theBoard, BufferedReader input, PrintStream out, AbstractShipFactory<Character> factory) {
    super(player, theBoard, input, out, factory);
  }
  @Override
  public void doPlacementPhase() throws IOException {
    // display the starting (empty) board
    out.print(view.displayMyOwnBoard());
    // print the instructions message
    String instructions = "Player " + TextPlayer + ": you are going to place the following ships (which are all"
        + "rectangular). For each ship, type the coordinate of the upper left"
        + "side of the ship, followed by either H (for horizontal) or V (for"
        + "vertical).  For example M4H would place a ship horizontally starting"
        + "at M4 and going to the right.  You have" + "\n" + "2 \"Submarines\" ships that are 1x2\n"
        + "3 \"Destroyers\" that are 1x3\n" + "3 \"Battleships\" that are T shape\n" + "2 \"Carriers\" that are Z shape\n";
    out.print(instructions);
    // cal doOnePlacement to place one ship
    for (String shipName: shipsToPlace) {
      String mes = doOnePlacement(shipName, shipCreationFns.get(shipName));
      while (mes != null) {
        out.print(mes);
        mes = doOnePlacement(shipName, shipCreationFns.get(shipName));
      }
    }
  }

  @Override
  public void playOneTurn(TextPlayer enemy) throws IOException {
    maker.playOneTurn(this, enemy);
  }


  public Placement readPlacement(String prompt) throws IOException, IllegalArgumentException {
    out.println(prompt);
    String s = inputReader.readLine();
    if (s == null)
      return null;
    return new Placement(s);
  }
  
  public String doOnePlacement(String shipName, Function<Placement, Ship<Character>> createFn) throws IOException {
    Placement p;
    try {
      p = readPlacement("Player " + TextPlayer + " where do you want to place a " + shipName + "?");
    } catch (IllegalArgumentException e) {
      return e.getMessage();
    }
    if (p == null) {
      throw new EOFException("the input message end, could not read from there\n");
    }
    Ship<Character> s;
    try {
      s = createFn.apply(p);
    }
    catch (IllegalArgumentException e) {
      return e.getMessage();
    }
    String mes = theBoard.tryAddShip(s);
    if (mes != null)
      return mes;
    out.print(view.displayMyOwnBoard());
    return null;
  }

  
  
  
}
