package edu.duke.xh110.battleship;

import java.util.HashMap;
import java.util.HashSet;

public class TShapedShip<T> extends BasicShip<T> {
  private final String name;
  public TShapedShip(Placement where, String name, SimpleShipDisplayInfo<T> myInfo, SimpleShipDisplayInfo<T> enemyInfo) {
    super(makeCoords(where.getWhere(), where.getOrientation()), myInfo, enemyInfo, where.getWhere(), setOrientation(where.getOrientation()),
          getHeight(where.getOrientation()),
          getWidth(where.getOrientation()));
    this.name = name;
  }

  public static int setOrientation(char o) {
    if (o == 'U') {
      return 0;
    } else if (o == 'R') {
      return 1;
    } else if (o == 'D') {
      return 2;
    } else {
      return 3;
    }
  }

  public static int getHeight(char orientation) {
    if (orientation == 'U' || orientation == 'D') return 2;
    else return 3;
  }

  public static int getWidth(char orientation) {
    if (orientation == 'U' || orientation == 'D') return 3;
    else return 2;
  }


  
  public TShapedShip(Placement where, String name, T mydata, T onHit) {
    this(where, name, new SimpleShipDisplayInfo<T>(mydata, onHit), new SimpleShipDisplayInfo<T>(null, mydata));
  }

  @Override
  public String getName() {
    return name;
  }


  public static HashSet<Coordinate> makeCoords(Coordinate upperLeft, char orientation) {
    /**
     * This method should generate the set of coordinates
     * for a rectangle starting at upperLeft whose width
     * and height are specified.
     */
    HashMap<Character, int[][]> map = new HashMap<Character,int[][]>();
    int[][] arr1 = {
      {0,1,0},
      {1,1,1},
    };
    map.put('U', arr1);
    int[][] arr2 = {
      {1,0},
      {1,1},
      {1,0}
    };
    map.put('R', arr2);
    int[][] arr3 = {
      {1,1,1},
      {0,1,0},
    };
    map.put('D', arr3);
    int[][] arr4 = {
      {0,1},
      {1,1},
      {0,1}
    };
    map.put('L', arr4);
    HashSet<Coordinate> set = new HashSet<Coordinate>();
    int[][] cur_arr = map.get(orientation);
    for (int i = 0; i < cur_arr.length; i++) {
      for (int j = 0; j < cur_arr[0].length; j++) {
        if (cur_arr[i][j] == 1)
          set.add(new Coordinate(i + upperLeft.getRow(), j + upperLeft.getColumn()));
      }
    }
    return set;
  }
  

}
