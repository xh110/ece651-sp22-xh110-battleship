/**
 *Constructs a BattleShipBoard with the specified width
 *and height
 *@param w is the width of the newly constructed board.
 *@param h is the height of the newly constructed board.
 *throws IllegalArgumentException if the width or height are less than or equal to zero
 */
package edu.duke.xh110.battleship;

import java.util.ArrayList;
import java.util.HashSet;

import com.google.errorprone.annotations.ForOverride;

public class BattleShipBoard<T> implements Board<T> {
  private final ArrayList<Ship<T>> myShips;
  private final int width;
  private final int height;
  private final PlacementRuleChecker<T> placementChecker;
  private final HashSet<Coordinate> enemyMisses;
  private final T missInfo;
  private ArrayList<ArrayList<T>> enemyBoard;
  
  public BattleShipBoard(int w, int h, PlacementRuleChecker<T> placementRuleChecker, T missInfo) {
    if (w <= 0) {
      throw new IllegalArgumentException("BattleShipBoard's width must be positive but is" + w);
    }
    if (h <= 0) {
      throw new IllegalArgumentException("BattleShipBoard's height must be positive but is" + h);
    }
    myShips = new ArrayList<Ship<T>>();
    this.width = w;
    this.height = h;
    this.placementChecker = placementRuleChecker;
    this.enemyMisses = new HashSet<Coordinate>();
    this.missInfo = missInfo;
    this.enemyBoard = new ArrayList<>();
    for (int i = 0; i < h; i++) {
      ArrayList<T> temp = new ArrayList<>();
      for (int j = 0; j < w; j++) {
        temp.add(null);
      }
      enemyBoard.add(temp);
    }
  }


  public BattleShipBoard(int w, int h, T missInfo) {
    this(w, h, makeRuleChain(), missInfo);
  }


  public static <T> PlacementRuleChecker<T> makeRuleChain() {
    PlacementRuleChecker<T>checker2 = new NoCollisionRuleChecker<T>(null);
    PlacementRuleChecker<T> checker1 = new InBoundsRuleChecker<T>(checker2);
    return checker1;
  }
  
  /*
   * add Ship to Board
   */
  public String tryAddShip(Ship<T> toAdd) {
    String mes = placementChecker.checkPlacement(toAdd, this);
    if (mes != null)
      return mes;
    if (!myShips.contains(toAdd))
      myShips.add(toAdd);
    return null;
  }
  
  public boolean isLose() {
    for (Ship<T> s: myShips) {
      if (!s.isSunk())
        return false;
    }
    return true;
  }


  public Ship<T> fireAt(Coordinate c) {
    for (Ship<T> s: myShips) {
      if (s.occupiesCoordinates(c)) {
        // found the ship, record the hit
        s.recordHitAt(c);
        enemyBoard.get(c.getRow()).set(c.getColumn(),s.getDisplayInfoAt(c, false));
        return s;
      }
    }
    // did not find, record the misses
    enemyMisses.add(c);
    enemyBoard.get(c.getRow()).set(c.getColumn(), missInfo);
    return null;
  }

  @Override
  public Ship<T> whatShipIsAt(Coordinate where) {
    for (Ship<T> s: myShips) {
      if (s.occupiesCoordinates(where)) {
        return s;
      }
    }
    return null;
  }

  @Override
  public ArrayList<Ship<T>> whatShipListAt(Coordinate where) {
    ArrayList<Ship<T>> arr = new ArrayList<>();
    for (Ship<T> s: myShips) {
      if (s.occupiesCoordinates(where)) {
        arr.add(s);
      }
    }
    return arr;
  }

  protected T whatIsAt(Coordinate where, boolean isSelf) {
    if (isSelf) {
      for (Ship<T> s : myShips) {
        if (s.occupiesCoordinates(where)) {
          return s.getDisplayInfoAt(where, isSelf);
        }
      }
      return null;
    }
    return enemyBoard.get(where.getRow()).get(where.getColumn());
  }
  public T whatIsAtForEnemy(Coordinate where) {
    return whatIsAt(where, false);
  }
  
  public T whatIsAtForSelf(Coordinate where) {
    return whatIsAt(where, true);
  }

  public int getWidth() {
    return width;
  }

  public int getHeight() {
    return height;
  }
}
