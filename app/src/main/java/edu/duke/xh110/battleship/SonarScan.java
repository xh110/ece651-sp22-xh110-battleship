package edu.duke.xh110.battleship;

import java.io.PrintStream;

public class SonarScan {
  public static String tryScan(Board<Character> enemyBoard, Coordinate c, PrintStream out) {
    String mes = checkSonar(enemyBoard, c);
    if (mes == null) {
      String ans = SonarScan.makeScan(enemyBoard, c);
      out.print(ans);
    }
    return mes;
  }

  public static String makeScan(Board<Character> board, Coordinate c) {
    int submarine = 0;
    int destroyer = 0;
    int battleship = 0;
    int carrier = 0;
    for (int i = -3; i <= 3; i++) {
      for (int j = 3 - Math.abs(i); j >= -3 + Math.abs(i); j--) {
        Coordinate cur = new Coordinate(c.getRow()+i, c.getColumn()+j);
        if (checkSonar(board, cur) == null) {
          Ship<Character> s = board.whatShipIsAt(cur);
          if (s != null) {
            submarine += s.getName().equals("Submarine") ? 1 : 0;
            destroyer += s.getName().equals("Destroyer") ? 1 : 0;
            battleship += s.getName().equals("Battleship") ? 1 : 0;
            carrier += s.getName().equals("Carrier") ? 1 : 0;            
          }
        }
      }
    }
    return SonarScan.getMes(submarine, destroyer, battleship, carrier);
  }

  public static String getMes(int s, int d, int b, int c) {
    String ans = "";
    ans += "Submarines occupy " + s + " squares\n";
    ans += "Destroyers occupy " + d + " squares\n";
    ans += "Battleships occupy " + b + " squares\n";
    ans += "Carriers occupy " + c + " squares\n";    
    return ans;
  }

  public static String checkSonar(Board<Character> theBoard, Coordinate c) {
    int w = c.getColumn();
    int h = c.getRow();
    int W = theBoard.getWidth();
    int H = theBoard.getHeight();
    String mes = null;
    if (h < 0 || h >= H || w < 0 || w >= W) {
      String mes_begin = "That Coordinate is invalid: the sornar point goes off the ";
      String mes_end = " of the board.\n";
      if (h < 0) {
        mes = mes_begin + "top" + mes_end;
      }
      else if (h >= H) {
        mes = mes_begin + "bottom" + mes_end;
      }
      else if (w < 0) {
        mes = mes_begin + "left" + mes_end;
      }
      else {
        mes = mes_begin + "right" + mes_end;
      }
    }
    return mes;
  }
  
}
