package edu.duke.xh110.battleship;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.IOException;
import java.io.PrintStream;

public class ActionMaker {
  private final BufferedReader inputReader;
  private final PrintStream out;
  int moveTime;
  int scanTime;

  public ActionMaker(BufferedReader input, PrintStream out, int moveTime, int scanTime) {
    this.inputReader = input;
    this.out = out;
    this.moveTime = moveTime;
    this.scanTime = scanTime;
  }

  public void playOneTurn(TextPlayer myplayer, TextPlayer enemyPlayer) throws IOException {
    myplayer.printTwoBoard(enemyPlayer);
    String action = null;
    do {
      printMessage(actionChoiceMes(myplayer)); {
        action = inputReader.readLine();
        try {
          if (action == null) throw new EOFException("End of input\n");
          else if (action.equals("F")) {
            break;
          }
          else if (action.equals("M")) {
            if (moveTime > 0)
              break;
            else printMessage("No more chance for movement\n");
          }
          else if (action.equals("S")) {
            if (scanTime > 0)
              break;
            else printMessage("No more chance for Sonar Scan\n");
          }
          else {
            printMessage("Invalid action\n");
          }
        }
        catch(EOFException e) {
          printMessage(e.getMessage());
          throw e;
        }
      }
    } while (true);

    // then try do perform action
    String mes = null;
    do {
      if (action.equals("S")) {
        mes = performScan(enemyPlayer);
      } else if (action.equals("F")) {
        mes = performFire(enemyPlayer);
      } else  {
        mes = performMove(myplayer);
      }
      if (mes != null)
        printMessage(mes);
    } while (mes != null);
  }

  public String performScan(TextPlayer enemy) throws IOException {
    printMessage("enter the Coordinate you want to scan\n");
    String action = inputReader.readLine();
    if (action == null) throw new EOFException("End of input\n");          
    Coordinate c = null;
    try {
      c = new Coordinate(action);
    } catch (IllegalArgumentException e) {
      return e.getMessage();
    }
    // at least minimun legal Coordinate
    String mes = SonarScan.tryScan(enemy.getBoard(), c, out);
    if (mes == null) scanTime -= 1;
    return mes;
  }

  public String performFire(TextPlayer enemy) throws IOException {
    printMessage("enter the Coordinate you want to fire\n");
    String action = inputReader.readLine();
    if (action == null) throw new EOFException("End of input\n");          
    Coordinate c = null;
    try {
      c = new Coordinate(action);
    } catch (IllegalArgumentException e) {
      return e.getMessage();
    }
    // at least minimun legal Coordinate
    String mes = MakeFire.tryMakeFire(enemy.getBoard(), c, out);
    return mes;
  }
  
  public String performMove(TextPlayer myplayer) throws IOException {
    printMessage("enter the Coordinate you want to pick a ship\n");
    String action = inputReader.readLine();
    if (action == null) throw new EOFException("End of input\n");          
    Coordinate c = null;
    Placement p = null;
    try {
      c = new Coordinate(action);
    } catch (IllegalArgumentException e) {
      return e.getMessage();
    }
    // then enter the placement
    printMessage("enter the Placement you want to move the ship\n");
    action = inputReader.readLine();
    if (action == null) throw new EOFException("End of input\n");          
    // check the validation of the placement
    try {
      p = new Placement(action);
    } catch (IllegalArgumentException e) {
      return e.getMessage();
    }
    // try to move
    String mes = MakeMove.moveShipProcess(myplayer.getBoard(), c, p);
    if (mes == null) moveTime -= 1;
    return mes;
  }

  public void printMessage(String prompt) {
    out.print(prompt);
  }


  public String actionChoiceMes(TextPlayer myplayer) {
    String mes = "Possible action for Player " + myplayer.playerName() + ":\n" +
      " F Fire at a square\n";
    if (moveTime > 0)
      mes +=" M Move a ship to another square (" + moveTime + " remaining)\n";
    if (scanTime > 0)
      mes += " S Sonar scan (" + scanTime + " remaining)\n\n";
    mes += "Player " + myplayer.playerName() + ", what would you like to do\n";
    return mes;
  }

}
