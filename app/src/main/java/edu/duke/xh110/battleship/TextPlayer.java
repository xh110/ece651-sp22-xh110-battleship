package edu.duke.xh110.battleship;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.function.Function;

public class TextPlayer {
  protected final Board<Character> theBoard;
  protected final BoardTextView view;
  protected final BufferedReader inputReader;
  protected final PrintStream out;
  protected final AbstractShipFactory<Character> shipFactory;
  protected final String TextPlayer;
  protected final ArrayList<String> shipsToPlace;
  protected final HashMap<String, Function<Placement, Ship<Character>>> shipCreationFns;
  protected final ActionMaker maker;


  public TextPlayer(String player, Board<Character> theBoard, BufferedReader input, PrintStream out,
      AbstractShipFactory<Character> factory) {
    this.shipFactory = factory;
    this.theBoard = theBoard;
    this.view = new BoardTextView(theBoard);
    this.inputReader = input;
    this.out = out;
    this.TextPlayer = player;
    this.shipsToPlace = new ArrayList<String>();
    this.shipCreationFns = new HashMap<String, Function<Placement, Ship<Character>>>();
    setupShipCreationList(2,3,3,2);
    setupShipCreationMap();
    this.maker = new ActionMaker(input, out, 2, 1);
  }

  protected void setupShipCreationMap() {
    shipCreationFns.put("Submarine", (p) -> shipFactory.makeSubmarine(p));
    shipCreationFns.put("Destroyer", (p) -> shipFactory.makeDestroyer(p));
    shipCreationFns.put("Battleship", (p) -> shipFactory.makeBattleship(p));
    shipCreationFns.put("Carrier", (p) -> shipFactory.makeCarrier(p));
  }

  protected void setupShipCreationList(int s, int d, int b, int c) {
    shipsToPlace.addAll(Collections.nCopies(s, "Submarine"));
    shipsToPlace.addAll(Collections.nCopies(d, "Destroyer"));
    shipsToPlace.addAll(Collections.nCopies(b, "Battleship"));
    shipsToPlace.addAll(Collections.nCopies(c, "Carrier"));
  }

  public void doPlacementPhase() throws IOException {}



  public boolean isLose() {
    return theBoard.isLose();
  }

  public void win_mes() {
    out.print("Player " + this.TextPlayer + "'s win!\n");
  }


  public void playOneTurn(TextPlayer enemy) throws IOException {}

  public void printTwoBoard(TextPlayer enemy) {
    out.print("Player " + TextPlayer + "'s turn\n");
    out.print(view.displayMyBoardWithEnemyNextToIt(enemy.view, "Your ocean", "Player " + enemy.TextPlayer + "'s ocean\n"));    
  }
  

  public String playerName() {return TextPlayer;}
  public Board<Character> getBoard() {return theBoard;}
  public BufferedReader getInput () {return inputReader;}
  public PrintStream getOut() {return out;}

}
