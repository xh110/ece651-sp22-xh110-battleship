package edu.duke.xh110.battleship;

import java.util.ArrayList;

public interface Board<T> {
  public int getWidth();
  public int getHeight();
  public String tryAddShip(Ship<T> toAdd);
  public T whatIsAtForSelf(Coordinate where);
  public T whatIsAtForEnemy(Coordinate where);
  public Ship<T> fireAt(Coordinate c);
  public boolean isLose();
  public Ship<T> whatShipIsAt(Coordinate where);
  public ArrayList<Ship<T>> whatShipListAt(Coordinate where);
}
