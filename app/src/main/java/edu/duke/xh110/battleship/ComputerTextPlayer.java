package edu.duke.xh110.battleship;

import java.io.IOException;
import java.io.PrintStream;
import java.util.Random;
import java.util.function.Function;

public class ComputerTextPlayer extends TextPlayer {
  private final Random random;
  public ComputerTextPlayer(String player, Board<Character> theBoard,  PrintStream out, AbstractShipFactory<Character> factory) {
    super(player, theBoard, null, out, factory);
    this.random = new Random(50);
  }
  @Override
  public void doPlacementPhase() throws IOException {
    String[] ships = {"a0h", "b0h", "c0h", "d0h", "e0h",
                      "f0u", "h0u", "j0r", "l0r", "n0r"};
    int i = 0;
    for (String shipName: shipsToPlace) {
      doOnePlacement(new Placement(ships[i]),shipName,shipCreationFns.get(shipName));
      i++;
    }
  }
  @Override
  public void playOneTurn(TextPlayer enemy) throws IOException {
    int row = random.nextInt(theBoard.getHeight());
    int col = random.nextInt(theBoard.getWidth());
    Coordinate where = new Coordinate(row, col);
    Ship<Character> s = theBoard.whatShipIsAt(where);
    if (s == null) {
      out.print("Computer " + TextPlayer + " missed!\n");
    } else {
      out.print("Computer " + TextPlayer + " hit a " + s.getName() + "!\n");
    }
    enemy.getBoard().fireAt(where);
  }

  public void doOnePlacement(Placement p, String shipName, Function<Placement, Ship<Character>> createFn) throws IOException {
    Ship<Character> s = null;
    s = createFn.apply(p);
    theBoard.tryAddShip(s);
  }



}
