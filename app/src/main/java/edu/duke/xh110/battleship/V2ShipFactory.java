package edu.duke.xh110.battleship;

public class V2ShipFactory implements AbstractShipFactory<Character> {
  protected Ship<Character> createRectangleShip(Placement where, int w, int h, char letter, String name) {
    char orientation = Character.toUpperCase(where.getOrientation());
    Coordinate upperLeft = where.getWhere();
    if (orientation != 'V' && orientation != 'H')
      throw new IllegalArgumentException("Invalid Orientation!\n");
    if (orientation == 'V') // no reverse
      return new RectangleShip<Character>(name, upperLeft, w, h, letter, '*', orientation);
    else //reverse
      return new RectangleShip<Character>(name, upperLeft, h, w, letter, '*', orientation);
  }

  
  protected Ship<Character> createV2Ship(Placement where, char letter, String name) throws IllegalArgumentException {
    char o = where.getOrientation();
    o = Character.toLowerCase(o);
    if (o != 'u' && o != 'r' && o != 'd' && o != 'l') {
      throw new IllegalArgumentException("Invalid orientation\n");
    }
    if (name.equals("Battleship")) {
      return new TShapedShip<Character>(where, name, letter, '*');
    } else {
      return new ZShapedShip<Character>(where, name, letter, '*');
    }
  }
  
  

  
  @Override
  public Ship<Character> makeSubmarine(Placement where) {
    return createRectangleShip(where, 1, 2, 's', "Submarine");
  }
  @Override
  public Ship<Character> makeDestroyer(Placement where) {
    return createRectangleShip(where, 1, 3, 'd', "Destroyer");
  }
  


  @Override
  public Ship<Character> makeBattleship(Placement where) {
    return createV2Ship(where, 'b', "Battleship");
  }

  @Override
  public Ship<Character> makeCarrier(Placement where) {
    return createV2Ship(where, 'c', "Carrier");
  }

}
