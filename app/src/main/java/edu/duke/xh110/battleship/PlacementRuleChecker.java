package edu.duke.xh110.battleship;

public abstract class PlacementRuleChecker<T> {
  private final PlacementRuleChecker<T> next;
  // more stuff
  public PlacementRuleChecker(PlacementRuleChecker<T> next) {
    this.next = next;
  }

  protected abstract String checkMyRule(Ship<T> theShip, Board<T> theBoard);

  public String checkPlacement (Ship<T> theShip, Board<T> theBoard) {
    //if fail our own rule: stop the placement is not legal
    String mes = checkMyRule(theShip, theBoard);
    if (mes != null) {
      return mes;
    }
    //otherwise, ask the rest of the chan
    if (next != null) {
      return next.checkPlacement(theShip, theBoard);
    }
    //if there are no more rules, then the placement is legal
    return null;
  }
}
