package edu.duke.xh110.battleship;

public class Coordinate {
  private final int row;
  private final int column;

  public Coordinate(int row, int column) {
    this.row = row;
    this.column = column;
  }

  public Coordinate(String descr) {
    if (descr.length() != 2) {
      throw new IllegalArgumentException("The argument should contain 2 characters\n");
    }
    char r = Character.toUpperCase(descr.charAt(0));
    if (!('A' <= r && r <= 'Z') && !('a' <= r && r <= 'a')) {
      throw new IllegalArgumentException("The row should be letter\n");
    }
    char c = descr.charAt(1);
    if (!('0' <= c && c <= '9')) {
      throw new IllegalArgumentException("The col should be digit\n");
    }
    this.row = (int)(r - 'A');
    this.column = (int)(c - '0');
  }

  public int getRow() {
    return row;
  }

  public int getColumn() {
    return column;
  }
  
  @Override
  public boolean equals(Object o) {
    if (o.getClass().equals(getClass())) {
      Coordinate c = (Coordinate) o;
      return row == c.row && column == c.column;
    }
    return false;
  }
  @Override
  public String toString() {
    return "(" + row + ", " + column + ")";
  }
  @Override
  public int hashCode() {
    return toString().hashCode();
  }
  
  
  
}
