package edu.duke.xh110.battleship;

import java.util.HashMap;

public class MakeMove {
  public static Ship<Character> pickAship(Board<Character> board, Coordinate location) {
    String mes = MakeMove.checkPick(board, location);
    if (mes == null) {
      return board.whatShipIsAt(location);
    } else {
      throw new IllegalArgumentException(mes);
    }
  }

  public static String moveAship(Board<Character> board, Ship<Character> ship, Placement where) {
    // check orientain validation
    String mes = checkOrientation(ship.getName(), where.getOrientation());
    if (mes != null) return mes;
    mes = tryMove(board, ship, where);
    return mes;
  }


  public static String moveShipProcess(Board<Character> board, Coordinate location, Placement dest) {
    Ship<Character> ship = null;
    try {
      ship = MakeMove.pickAship(board, location);
    } catch(IllegalArgumentException e) {
      return e.getMessage();
    }
    if (ship == null) return "There is no ship for movement\n";
    return moveAship(board, ship, dest);
 }

  public static String tryMove(Board<Character> board, Ship<Character> ship, Placement where) {
    // copy the oldPieces
    HashMap<Coordinate, Boolean> oldPieces = copyPieces(ship.getPieces());
    int oldOri = ship.getOrientation();
    int oldRow = ship.getRow();
    int oldCol = ship.getCol();
    Coordinate upperLeft = ship.getUpperLeft();
    moveAndrotate(ship, where);
    String mes = board.tryAddShip(ship);
    if (mes != null) {
      // fail to move
      // need to reset everything
      ship.replacePieces(oldPieces);
      ship.modifyUpperLeft(where.getWhere());
      ship.setOrientation(oldOri);
      ship.changeCol(oldCol);
      ship.changeRow(oldRow);
      ship.modifyUpperLeft(upperLeft);
      return mes;
    }
    return null;
  }

  public static void moveAndrotate(Ship<Character> ship, Placement where) {
    moveFirst(ship, where.getWhere());
    int rotateTime = 0;
    char o = where.getOrientation();
    if (o == 'H' || o == 'U') rotateTime = 0;
    else if (o == 'V' || o == 'R') rotateTime = 1;
    else if (o == 'D') rotateTime = 2;
    else rotateTime = 3;
    rotateThen(ship, (rotateTime - ship.getOrientation() + 4) % 4);
  }

  public static void rotateThen(Ship<Character> ship, int time) {
    Coordinate begin = ship.getUpperLeft();
    int row = begin.getRow();
    int col = begin.getColumn();
    for (int i = 0; i < time; i++) {
      HashMap<Coordinate, Boolean> pieces = ship.getPieces();
      HashMap<Coordinate, Boolean> replace = new HashMap<>();
      for (Coordinate c: pieces.keySet()) {
        replace.put(new Coordinate(row - col + c.getColumn(),
                                   col + row - c.getRow() + ship.getRow() - 1),
                    pieces.get(c));
      }
      ship.replacePieces(replace);
      // swap the row and col
      int temp = ship.getRow();
      ship.changeRow(ship.getCol());;
      ship.changeCol(temp);
      // change the orientation
      int oldOri = (ship.getOrientation() + 1) % 4;
      ship.setOrientation(oldOri);
    }
  }

  public static void moveFirst(Ship<Character> ship, Coordinate where) {
    Coordinate begin = ship.getUpperLeft();
    int h = where.getRow() - begin.getRow();
    int w = where.getColumn() - begin.getColumn();
    HashMap<Coordinate, Boolean> pieces = ship.getPieces();
    HashMap<Coordinate, Boolean> replace = new HashMap<>();
    for (Coordinate c: pieces.keySet()) {
      replace.put(new Coordinate(c.getRow() + h, c.getColumn() + w), pieces.get(c));
    }
    ship.replacePieces(replace);
    ship.modifyUpperLeft(where);
  }

  public static HashMap<Coordinate, Boolean> copyPieces(HashMap<Coordinate, Boolean> Pieces) {
    HashMap<Coordinate, Boolean> cpPieces = new HashMap<>();
    for (Coordinate c: Pieces.keySet()) {
      cpPieces.put(new Coordinate(c.getRow(),c.getColumn()), Pieces.get(c));
    }
    return cpPieces;
  }
  public static String checkPick(Board<Character> theBoard, Coordinate c) {
    int w = c.getColumn();
    int h = c.getRow();
    int W = theBoard.getWidth();
    int H = theBoard.getHeight();
    String mes = null;
    if (h < 0 || h >= H || w < 0 || w >= W) {
      String mes_begin = "That Coordinate is invalid: the location goes off the ";
      String mes_end = " of the board.\n";
      if (h < 0) {
        mes = mes_begin + "top" + mes_end;
      }
      else if (h >= H) {
        mes = mes_begin + "bottom" + mes_end;
      }
      else if (w < 0) {
        mes = mes_begin + "left" + mes_end;
      }
      else {
        mes = mes_begin + "right" + mes_end;
      }
    }
    return mes;
  }

  public static String checkOrientation(String name, char o) {
    if (name.equals("Submarine") || name.equals("Destroyer")) {
      if (o != 'V' && o != 'H') return "Invalid orientation\n";
    }
    else if (name.equals("Battleship") || name.equals("Carrier")) {
      if (o != 'U' && o != 'R' && o != 'D' && o != 'L')
        return "Invalid orientation\n";
    }
    else return "Invalid ship name\n";
    return null;
  }


  
}
