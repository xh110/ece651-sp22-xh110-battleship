package edu.duke.xh110.battleship;

import java.io.PrintStream;

public class MakeFire {
  public static String tryMakeFire(Board<Character> enemyBoard, Coordinate c, PrintStream out) {
    // check if the coordinate is out of bound
    String mes = checkFireAt(enemyBoard, c);
    if (mes == null) {
      Ship<Character> s = enemyBoard.fireAt(c);
      if (s == null) out.print("You missed!\n");
      else out.print("You hit a " + s.getName() + "!\n"); 
    }
    return mes;
  }

  public static String checkFireAt(Board<Character> theBoard, Coordinate c) {
    int w = c.getColumn();
    int h = c.getRow();
    int W = theBoard.getWidth();
    int H = theBoard.getHeight();
    String mes = null;
    if (h < 0 || h >= H || w < 0 || w >= W) {
      String mes_begin = "That Coordinate is invalid: the fire goes off the ";
      String mes_end = " of the board.\n";
      if (h < 0) {
        mes = mes_begin + "top" + mes_end;
      }
      else if (h >= H) {
        mes = mes_begin + "bottom" + mes_end;
      }
      else if (w < 0) {
        mes = mes_begin + "left" + mes_end;
      }
      else {
        mes = mes_begin + "right" + mes_end;
      }
    }
    return mes;
  }
  
}
