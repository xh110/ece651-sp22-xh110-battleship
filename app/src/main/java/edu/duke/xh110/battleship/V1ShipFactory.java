package edu.duke.xh110.battleship;

public class V1ShipFactory implements AbstractShipFactory<Character> {
  protected Ship<Character> createShip(Placement where, int w, int h, char letter, String name) {
    char orientation = Character.toUpperCase(where.getOrientation());
    Coordinate upperLeft = where.getWhere();
    if (orientation != 'V' && orientation != 'H')
      throw new IllegalArgumentException("Invalid Orientation!\n");
    if (orientation == 'V') // no reverse
      return new RectangleShip<Character>(name, upperLeft, w, h, letter, '*', 'V');
    else //reverse
      return new RectangleShip<Character>(name, upperLeft, h, w, letter, '*', 'H');
  }
  
  @Override
  public Ship<Character> makeSubmarine(Placement where) {
    return createShip(where, 1, 2, 's', "Submarine");
  }

  @Override
  public Ship<Character> makeBattleship(Placement where) {
    return createShip(where, 1, 4, 'b', "Battleship");
  }

  @Override
  public Ship<Character> makeDestroyer(Placement where) {
    return createShip(where, 1, 3, 'd', "Destroyer");
  }

  @Override
  public Ship<Character> makeCarrier(Placement where) {
    return createShip(where, 1, 6, 'c', "Carrier");
  }

}
