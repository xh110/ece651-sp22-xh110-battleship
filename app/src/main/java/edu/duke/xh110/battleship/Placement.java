
package edu.duke.xh110.battleship;

public class Placement {
  private final Coordinate where;
  private final char orientation;

  public Placement(String s) {
    if (s.length() != 3) {
      throw new IllegalArgumentException("The string should contain 3 characters\n");
    }
    this.where = new Coordinate(s.substring(0, 2));
    this.orientation = Character.toUpperCase(s.charAt(2));
    if (!Character.isLetter(orientation)) {
      throw new IllegalArgumentException("The orientation should be letter\n");
    }

  }

  public Placement(Coordinate where, char orientation) {
    if (!Character.isLetter(orientation)) {
      throw new IllegalArgumentException("The orientation should be letter\n");
    }
    this.where = where;
    this.orientation = Character.toUpperCase(orientation);
  }

  public Coordinate getWhere() {
    return this.where;
  }

  public char getOrientation() {
    return this.orientation;
  }

  @Override
  public String toString() {
    return where.toString() + orientation;
  }

  @Override
  public boolean equals(Object o) {
    if (o.getClass().equals(getClass())) {
      Placement c = (Placement) o;
      return c.where.equals(where) && c.orientation == orientation;
    }
    return false;
  }

  @Override
  public int hashCode() {
    return toString().hashCode();
  }

}
