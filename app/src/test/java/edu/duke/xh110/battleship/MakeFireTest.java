package edu.duke.xh110.battleship;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.Test;

public class MakeFireTest {
  @Test
  void test_tryMakeFire() throws IllegalArgumentException {
    Board<Character> board = new BattleShipBoard<Character>(10, 10, 'X');
    assertNotEquals(null, MakeFire.tryMakeFire(board, new Coordinate(-1, 0),  System.out));
    assertNotEquals(null, MakeFire.tryMakeFire(board, new Coordinate(0, -1),  System.out));
    assertNotEquals(null, MakeFire.tryMakeFire(board, new Coordinate(30, 0),  System.out));
    assertEquals(null, MakeFire.tryMakeFire(board, new Coordinate(5, 5), System.out));
  }
  

  @Test
  void test_checkFireAt() throws IllegalArgumentException {
    Board<Character> board = new BattleShipBoard<Character>(10, 10, 'X');
    assertNotEquals(null, MakeFire.checkFireAt(board, new Coordinate(-1, 0)));
    assertNotEquals(null, MakeFire.checkFireAt(board, new Coordinate(0, -1)));
    assertNotEquals(null, MakeFire.checkFireAt(board, new Coordinate(30, 0)));
    assertNotEquals(null, MakeFire.checkFireAt(board, new Coordinate(0, 30)));
    assertEquals(null, MakeFire.checkFireAt(board, new Coordinate(5, 5)));
  }
}
