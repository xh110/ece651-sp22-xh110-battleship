package edu.duke.xh110.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class MakeMoveTest {
  @Test
  public void test_makeShipProcess() {
    Board<Character> board = new BattleShipBoard<Character>(10, 10, 'X');
    Placement p1 = new Placement(new Coordinate(1, 2), 'U');
    Ship<Character> s1 = new TShapedShip<Character>(p1, "Battleship", 'b', '*');
    board.tryAddShip(s1);
    Placement dest = new Placement(new Coordinate(7,5), 'L');
    assertEquals(null, MakeMove.moveShipProcess(board, new Coordinate(1,3), dest));

    assertEquals(null, board.whatShipIsAt(new Coordinate(7,5)));
    assertEquals(s1, board.whatShipIsAt(new Coordinate(7,6)));
    assertEquals(s1, board.whatShipIsAt(new Coordinate(8,5)));
    assertEquals(s1, board.whatShipIsAt(new Coordinate(8,6)));
    assertEquals(null, board.whatShipIsAt(new Coordinate(9,5)));
    assertEquals(s1, board.whatShipIsAt(new Coordinate(9,6)));
    
  }
  @Test
  public void test_pickAship() {
    Board<Character> board = new BattleShipBoard<Character>(10, 10, 'X');
    Placement p1 = new Placement(new Coordinate(1, 2), 'U');
    Ship<Character> s1 = new TShapedShip<Character>(p1, "Battleship", 'b', '*');
    board.tryAddShip(s1);
    assertEquals(null, MakeMove.pickAship(board, new Coordinate(1,2)));
    assertEquals(s1, MakeMove.pickAship(board, new Coordinate(1,3)));
    assertEquals(null, MakeMove.pickAship(board, new Coordinate(1,4)));
    assertEquals(s1, MakeMove.pickAship(board, new Coordinate(2,2)));
    assertEquals(s1, MakeMove.pickAship(board, new Coordinate(2,3)));
    assertEquals(s1, MakeMove.pickAship(board, new Coordinate(2,4)));    
  }
  @Test
  public void test_tryMove() {
    Board<Character> board = new BattleShipBoard<Character>(10, 10, 'X');
    Placement p1 = new Placement(new Coordinate(1, 2), 'U');
    Ship<Character> s1 = new TShapedShip<Character>(p1, "Battleship", 'b', '*');
    board.tryAddShip(s1);
    MakeMove.tryMove(board, s1, new Placement(new Coordinate(7, 5), 'L'));
    assertEquals(null, board.whatShipIsAt(new Coordinate(7,5)));
    assertEquals(s1, board.whatShipIsAt(new Coordinate(7,6)));
    assertEquals(s1, board.whatShipIsAt(new Coordinate(8,5)));
    assertEquals(s1, board.whatShipIsAt(new Coordinate(8,6)));
    assertEquals(null, board.whatShipIsAt(new Coordinate(9,5)));
    assertEquals(s1, board.whatShipIsAt(new Coordinate(9,6)));

    Placement p2 = new Placement(new Coordinate(4, 4), 'U');
    Ship<Character> s2 = new TShapedShip<Character>(p2, "Battleship", 'b', '*');
    assertEquals(null,board.tryAddShip(s2));

    assertEquals(null, board.whatShipIsAt(new Coordinate(4,4)));
    assertEquals(true, s2.occupiesCoordinates(new Coordinate(4,5)));
    assertEquals(s2, board.whatShipIsAt(new Coordinate(4,5)));
    assertEquals(null, board.whatShipIsAt(new Coordinate(4,6)));

    Placement dest = new Placement(new Coordinate(7, 5), 'L');
    assertNotEquals(null, MakeMove.tryMove(board, s2, dest));


    assertEquals(null, board.whatShipIsAt(new Coordinate(4,4)));
    assertEquals(s2, board.whatShipIsAt(new Coordinate(4,5)));
    assertEquals(null, board.whatShipIsAt(new Coordinate(4,6)));
    assertEquals(s2, board.whatShipIsAt(new Coordinate(5,4)));
    assertEquals(s2, board.whatShipIsAt(new Coordinate(5,5)));
    assertEquals(s2, board.whatShipIsAt(new Coordinate(5,6)));    

  }
  
  @Test
  public void test_moveFirst() {
    Placement p1 = new Placement(new Coordinate(1, 2), 'U');
    Ship<Character> s1 = new TShapedShip<Character>(p1, "Battleship", 'b', '*');

    MakeMove.moveFirst(s1, new Coordinate(7,5));
    assertEquals(false, s1.occupiesCoordinates(new Coordinate(7,5)));
    assertEquals(true, s1.occupiesCoordinates(new Coordinate(7,6)));
    assertEquals(false, s1.occupiesCoordinates(new Coordinate(7,7)));
    assertEquals(true, s1.occupiesCoordinates(new Coordinate(8,5)));
    assertEquals(true, s1.occupiesCoordinates(new Coordinate(8,6)));
    assertEquals(true, s1.occupiesCoordinates(new Coordinate(8,7)));
  }


  @Test
  public void test_rotateThen() {
    Placement p1 = new Placement(new Coordinate(7,5), 'U');
    Ship<Character> s1 = new TShapedShip<Character>(p1, "Battleship", 'b', '*');

    /*
    MakeMove.rotateThen(s1, 1);

    assertEquals(true, s1.occupiesCoordinates(new Coordinate(7,5)));
    assertEquals(false, s1.occupiesCoordinates(new Coordinate(7,6)));
    assertEquals(true, s1.occupiesCoordinates(new Coordinate(8,5)));
    assertEquals(true, s1.occupiesCoordinates(new Coordinate(8,6)));
    assertEquals(true, s1.occupiesCoordinates(new Coordinate(9,5)));
    assertEquals(false, s1.occupiesCoordinates(new Coordinate(9,6)));

    MakeMove.rotateThen(s1, 1);

    assertEquals(true, s1.occupiesCoordinates(new Coordinate(7,5)));
    assertEquals(true, s1.occupiesCoordinates(new Coordinate(7,6)));
    assertEquals(true, s1.occupiesCoordinates(new Coordinate(7,7)));
    assertEquals(false, s1.occupiesCoordinates(new Coordinate(8,5)));
    assertEquals(true, s1.occupiesCoordinates(new Coordinate(8,6)));
    assertEquals(false, s1.occupiesCoordinates(new Coordinate(8,7)));
    */

    MakeMove.rotateThen(s1, 3);
    assertEquals(false, s1.occupiesCoordinates(new Coordinate(7,5)));
    assertEquals(true, s1.occupiesCoordinates(new Coordinate(7,6)));
    assertEquals(true, s1.occupiesCoordinates(new Coordinate(8,5)));
    assertEquals(true, s1.occupiesCoordinates(new Coordinate(8,6)));
    assertEquals(false, s1.occupiesCoordinates(new Coordinate(9,5)));
    assertEquals(true, s1.occupiesCoordinates(new Coordinate(9,6)));
    
  }
  
  @Test
  public void test_moveAndrotate() {
    Placement p1 = new Placement(new Coordinate(1,2), 'U');
    Ship<Character> s1 = new TShapedShip<Character>(p1, "Battleship", 'b', '*');

    MakeMove.moveAndrotate(s1, new Placement(new Coordinate(7,5), 'L'));
    assertEquals(false, s1.occupiesCoordinates(new Coordinate(7,5)));
    assertEquals(true, s1.occupiesCoordinates(new Coordinate(7,6)));
    assertEquals(true, s1.occupiesCoordinates(new Coordinate(8,5)));
    assertEquals(true, s1.occupiesCoordinates(new Coordinate(8,6)));
    assertEquals(false, s1.occupiesCoordinates(new Coordinate(9,5)));
    assertEquals(true, s1.occupiesCoordinates(new Coordinate(9,6)));

    MakeMove.moveAndrotate(s1, new Placement(new Coordinate(7,5), 'R'));
    assertEquals(true, s1.occupiesCoordinates(new Coordinate(7,5)));
    assertEquals(false, s1.occupiesCoordinates(new Coordinate(7,6)));
    assertEquals(true, s1.occupiesCoordinates(new Coordinate(8,5)));
    assertEquals(true, s1.occupiesCoordinates(new Coordinate(8,6)));
    assertEquals(true, s1.occupiesCoordinates(new Coordinate(9,5)));
    assertEquals(false, s1.occupiesCoordinates(new Coordinate(9,6)));
    
    MakeMove.moveAndrotate(s1, new Placement(new Coordinate(7,5), 'D'));
    assertEquals(true, s1.occupiesCoordinates(new Coordinate(7,5)));
    assertEquals(true, s1.occupiesCoordinates(new Coordinate(7,6)));
    assertEquals(true, s1.occupiesCoordinates(new Coordinate(7,7)));
    assertEquals(false, s1.occupiesCoordinates(new Coordinate(8,5)));
    assertEquals(true, s1.occupiesCoordinates(new Coordinate(8,6)));
    assertEquals(false, s1.occupiesCoordinates(new Coordinate(8,7)));
    
    

  }
  
}
