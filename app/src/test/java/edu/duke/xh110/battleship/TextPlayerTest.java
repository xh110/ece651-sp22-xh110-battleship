package edu.duke.xh110.battleship;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.StringReader;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

public class TextPlayerTest {
  /*
  @Test void test_checkFireAt() throws IllegalArgumentException {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer player1 = createTextPlayer(10, 20, "B2V\nC8H\na4v\n", bytes);
    TextPlayer player2 = createTextPlayer(10, 20, "B2V\nC8H\na4v\n", bytes);
    assertThrows(IllegalArgumentException.class,() ->  player1.checkFireAt(new Coordinate(-1, 0), player2));
    assertThrows(IllegalArgumentException.class,() ->  player1.checkFireAt(new Coordinate(0, 30), player2));
    assertThrows(IllegalArgumentException.class,() ->  player1.checkFireAt(new Coordinate(30, 0), player2));
    assertThrows(IllegalArgumentException.class,() ->  player1.checkFireAt(new Coordinate(0, -1), player2));

  }
  @Test
  public void test_read_placement() throws IOException {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer player = createTextPlayer(10, 20, "B2V\nC8H\na4v\n", bytes);

    String prompt = "Please enter a location for a ship:";
    Placement[] expected = new Placement[3];
    expected[0] = new Placement(new Coordinate(1, 2), 'V');
    expected[1] = new Placement(new Coordinate(2, 8), 'H');
    expected[2] = new Placement(new Coordinate(0, 4), 'V');

    for (int i = 0; i < expected.length; i++) {
      Placement p = player.readPlacement(prompt);
      assertEquals(p, expected[i]);
      assertEquals(prompt + "\n", bytes.toString());
      bytes.reset();
    }
  }

  
  @Test
  public void test_doPlacement() throws IOException {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer player = createTextPlayer(10, 10, "a0h\nb0H\nc0h\nd0h\ne0H\nf0h\ng0h\nh0H\ni0h\nj0h\n", bytes);
    player.doPlacementPhase();
    Board<Character> theBoard = new BattleShipBoard<Character>(10, 10, 'X');
    BoardTextView view = new BoardTextView(theBoard);
    String expected = view.displayMyOwnBoard();
    expected += "Player A: you are going to place the following ships (which are all" 
        + "rectangular). For each ship, type the coordinate of the upper left"
        + "side of the ship, followed by either H (for horizontal) or V (for"
        + "vertical).  For example M4H would place a ship horizontally starting"
        + "at M4 and going to the right.  You have"
        + "\n"
        + "2 \"Submarines\" ships that are 1x2" 
        + "3 \"Destroyers\" that are 1x3"
        + "3 \"Battleships\" that are 1x4"
        + "2 \"Carriers\" that are 1x6";
    // actually no test here
    //assertEquals(expected, bytes.toString());
  }

    @Test
    public void test_EOFdoPlacement() throws IOException, EOFException{
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer player = createTextPlayer(10, 5, "a0h\nb0H\nc0h\nd0h\ne0H\nf0h\ng0h\n", bytes);
    assertThrows(EOFException.class,()-> player.doPlacementPhase());
  }



  private TextPlayer createTextPlayer(int w, int h, String inputData, OutputStream bytes) {
    BufferedReader input = new BufferedReader(new StringReader(inputData));
    PrintStream output = new PrintStream(bytes, true);
    Board<Character> board = new BattleShipBoard<Character>(w, h, 'X');
    V1ShipFactory shipFactory = new V1ShipFactory();
    return new TextPlayer("A", board, input, output, shipFactory);
  }

  
  @Test
  public void test_doOnePlacement() throws IOException {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer player = createTextPlayer(5, 2, "a0h\nb0h\nb0vv\nb00\n", bytes);

    // set expected string
    String[] expected = new String[2];
    String ques = "Player A where do you want to place a Destroyer?\n";
    String Header = "  0|1|2|3|4\n";
    expected[0] =
      ques +
      Header +
      "A d|d|d| |  A\n" +
      "B  | | | |  B\n" +
      Header;

    expected[1] =
      ques +
      Header +
      "A d|d|d| |  A\n" +
      "B d|d|d| |  B\n" +
      Header;
    
      
    // do the placement
    AbstractShipFactory<Character> f = new V1ShipFactory();
    String shipName = "Destroyer";
    for (int i = 0; i < expected.length; i++) {
      player.doOnePlacement(shipName, (p) -> f.makeDestroyer(p));
      String s = bytes.toString();
      assertEquals(expected[i], s);
      bytes.reset();
    }
    String mes = player.doOnePlacement(shipName, (p) -> f.makeDestroyer(p));
    assertEquals("The string should contain 3 characters\n", mes);
    mes = player.doOnePlacement(shipName, (p) -> f.makeDestroyer(p));
    assertEquals("The orientation should be letter\n", mes);

    assertThrows(EOFException.class,()-> player.doOnePlacement(shipName, (p) -> f.makeDestroyer(p)));
    // test the view each time after placement
  }
  */


}
