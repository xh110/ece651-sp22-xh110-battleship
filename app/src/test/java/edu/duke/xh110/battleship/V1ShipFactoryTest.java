package edu.duke.xh110.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class V1ShipFactoryTest {
  @Test
  public void test_() {
    V1ShipFactory f = new V1ShipFactory();

    // make a submarine
    Placement v1_1 = new Placement(new Coordinate(1, 2), 'V');
    Ship<Character> dst1 = f.makeSubmarine(v1_1);
    checkShip(dst1, "Submarine", 's', new Coordinate(1, 2), new Coordinate(2, 2));

    // make a destroyer
    Placement v1_2 = new Placement(new Coordinate(1, 2), 'V');
    Ship<Character> dst2 = f.makeDestroyer(v1_2);
    checkShip(dst2, "Destroyer", 'd', new Coordinate(1, 2), new Coordinate(2, 2), new Coordinate(3, 2));

    // make a battleship
    Placement v1_3 = new Placement(new Coordinate(1, 2), 'V');
    Ship<Character> dst3 = f.makeBattleship(v1_3);
    checkShip(dst3, "Battleship", 'b', new Coordinate(1, 2), new Coordinate(2, 2), new Coordinate(3, 2), new Coordinate(4, 2));

    // make a carrier
    Placement v1_4 = new Placement(new Coordinate(1, 2), 'V');
    Ship<Character> dst4 = f.makeCarrier(v1_4);
    checkShip(dst4, "Carrier", 'c', new Coordinate(1, 2), new Coordinate(2, 2), new Coordinate(3, 2),
              new Coordinate(4, 2), new Coordinate(5, 2), new Coordinate(6, 2));

    // make a H submarine
    Placement v1_1_h  = new Placement(new Coordinate(1, 2), 'H');
    Ship<Character> dst1_h = f.makeSubmarine(v1_1_h);
    checkShip(dst1_h, "Submarine", 's', new Coordinate(1, 2), new Coordinate(1, 3));

    // check invalid orientation
    Placement v1_5 = new Placement(new Coordinate(1, 2), 'K');
    assertThrows(IllegalArgumentException.class, ()->f.makeSubmarine(v1_5));
  }

  private void checkShip(Ship<Character>testShip, String expectedName,
                         char expectedLetter, Coordinate... expectedLocs) {
    // check name
    assertEquals(expectedName, testShip.getName());    
    // check location: occupy
    for (int i = 0; i < expectedLocs.length; i++) {
      assertEquals(true, testShip.occupiesCoordinates(expectedLocs[i]));
    }
    // check letter
    assertEquals(expectedLetter, testShip.getDisplayInfoAt(expectedLocs[0], true));

  }

}
