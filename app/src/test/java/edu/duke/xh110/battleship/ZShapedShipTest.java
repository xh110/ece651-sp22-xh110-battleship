package edu.duke.xh110.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class ZShapedShipTest {
  @Test
  public void test_ZShapedShip() {
    Placement p1 = new Placement(new Coordinate(3,4), 'u');
    Ship<Character> s1 = new ZShapedShip<>(p1, "Carrier", 'c', '*');
    assertEquals(true, s1.occupiesCoordinates(new Coordinate(3,4)));
    assertEquals(true, s1.occupiesCoordinates(new Coordinate(4,4)));
    assertEquals(true, s1.occupiesCoordinates(new Coordinate(5,4)));
    assertEquals(true, s1.occupiesCoordinates(new Coordinate(6,4)));
    assertEquals(false, s1.occupiesCoordinates(new Coordinate(7,4)));
    assertEquals(false, s1.occupiesCoordinates(new Coordinate(3,5)));
    assertEquals(false, s1.occupiesCoordinates(new Coordinate(4,5)));
    assertEquals(true, s1.occupiesCoordinates(new Coordinate(5,5)));
    assertEquals(true, s1.occupiesCoordinates(new Coordinate(6,5)));
    assertEquals(true, s1.occupiesCoordinates(new Coordinate(7,5)));

    
    Placement p2 = new Placement(new Coordinate(3,4), 'd');
    Ship<Character> s2 = new ZShapedShip<>(p2, "Carrier", 'c', '*');
    assertEquals(true, s2.occupiesCoordinates(new Coordinate(3,4)));
    assertEquals(true, s2.occupiesCoordinates(new Coordinate(4,4)));
    assertEquals(true, s2.occupiesCoordinates(new Coordinate(5,4)));
    assertEquals(false, s2.occupiesCoordinates(new Coordinate(6,4)));
    assertEquals(false, s2.occupiesCoordinates(new Coordinate(7,4)));
    assertEquals(false, s2.occupiesCoordinates(new Coordinate(3,5)));
    assertEquals(true, s2.occupiesCoordinates(new Coordinate(4,5)));
    assertEquals(true, s2.occupiesCoordinates(new Coordinate(5,5)));
    assertEquals(true, s2.occupiesCoordinates(new Coordinate(6,5)));
    assertEquals(true, s2.occupiesCoordinates(new Coordinate(7,5)));

    
    Placement p3 = new Placement(new Coordinate(3,4), 'r');
    Ship<Character> s3 = new ZShapedShip<>(p3, "Carrier", 'c', '*');
    assertEquals(false, s3.occupiesCoordinates(new Coordinate(3,4)));
    assertEquals(true, s3.occupiesCoordinates(new Coordinate(3,5)));
    assertEquals(true, s3.occupiesCoordinates(new Coordinate(3,6)));
    assertEquals(true, s3.occupiesCoordinates(new Coordinate(3,7)));
    assertEquals(true, s3.occupiesCoordinates(new Coordinate(3,8)));
    assertEquals(true, s3.occupiesCoordinates(new Coordinate(4,4)));    
    assertEquals(true, s3.occupiesCoordinates(new Coordinate(4,5)));
    assertEquals(true, s3.occupiesCoordinates(new Coordinate(4,6)));
    assertEquals(false, s3.occupiesCoordinates(new Coordinate(4,7)));    
    assertEquals(false, s3.occupiesCoordinates(new Coordinate(4,8)));
    
    Placement p4 = new Placement(new Coordinate(3,4), 'l');
    Ship<Character> s4 = new ZShapedShip<>(p4, "Carrier", 'c', '*');
    assertEquals(false, s4.occupiesCoordinates(new Coordinate(3,4)));
    assertEquals(false, s4.occupiesCoordinates(new Coordinate(3,5)));
    assertEquals(true, s4.occupiesCoordinates(new Coordinate(3,6)));
    assertEquals(true, s4.occupiesCoordinates(new Coordinate(3,7)));
    assertEquals(true, s4.occupiesCoordinates(new Coordinate(3,8)));
    assertEquals(true, s4.occupiesCoordinates(new Coordinate(4,4)));    
    assertEquals(true, s4.occupiesCoordinates(new Coordinate(4,5)));
    assertEquals(true, s4.occupiesCoordinates(new Coordinate(4,6)));
    assertEquals(true, s4.occupiesCoordinates(new Coordinate(4,7)));
    assertEquals(false, s4.occupiesCoordinates(new Coordinate(4,8)));

  }

  @Test
  public void test_getName() {
    Placement p4 = new Placement(new Coordinate(3,4), 'l');
    Ship<Character> s4 = new ZShapedShip<>(p4, "Carrier", 'c', '*');
    assertEquals("Carrier", s4.getName());
  }

  @Test
  public void test_getOrientation() {
    Placement p1 = new Placement(new Coordinate(3,4), 'u');
    Ship<Character> s1 = new ZShapedShip<>(p1, "Carrier", 'c', '*');
    assertEquals(0, s1.getOrientation());    

    
    Placement p2 = new Placement(new Coordinate(3,4), 'r');
    Ship<Character> s2 = new ZShapedShip<>(p2, "Carrier", 'c', '*');
    assertEquals(1, s2.getOrientation());    

    
    Placement p3 = new Placement(new Coordinate(3,4), 'd');
    Ship<Character> s3 = new ZShapedShip<>(p3, "Carrier", 'c', '*');
    assertEquals(2, s3.getOrientation());    

    Placement p4 = new Placement(new Coordinate(3,4), 'l');
    Ship<Character> s4 = new ZShapedShip<>(p4, "Carrier", 'c', '*');
    assertEquals(3, s4.getOrientation());    
  }
  @Test
  public void test_getUpperLeft() {
    Placement p1 = new Placement(new Coordinate(3,4), 'u');
    Ship<Character> s1 = new ZShapedShip<>(p1, "Carrier", 'c', '*');
    assertEquals(new Coordinate(3,4), s1.getUpperLeft());    
  }

}
