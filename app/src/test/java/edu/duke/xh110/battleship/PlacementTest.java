package edu.duke.xh110.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class PlacementTest {
  @Test
  public void test_Construct() {
    assertThrows(IllegalArgumentException.class, ()->new Placement("a4ha"));
    assertThrows(IllegalArgumentException.class, ()->new Placement("44h"));
    assertThrows(IllegalArgumentException.class, ()->new Placement("Aah"));
    assertThrows(IllegalArgumentException.class, ()->new Placement("a44"));
  
  }
  @Test
  public void test_equals() {
    Coordinate a1 = new Coordinate(1, 2);
    Coordinate a2 = new Coordinate(1, 2);
    Coordinate a3 = new Coordinate(2, 3);
    Placement p1 = new Placement(a1, 'v');
    Placement p2 = new Placement(a2, 'V');
    Placement p3 = new Placement(a3, 'V');
    assertEquals(p1, p2);
    assertNotEquals(p2, p3);
    assertNotEquals(p1, a1);
    assertThrows(IllegalArgumentException.class, ()->new Placement(a3, '1'));    
  }
  @Test
  void test_get() {
    Coordinate a1 = new Coordinate(1, 2);
    Coordinate a2 = new Coordinate(1, 2);
    Coordinate a3 = new Coordinate(2, 3);
    Placement p1 = new Placement(a1, 'v');
    Placement p2 = new Placement(a2, 'V');
    Placement p3 = new Placement(a3, 'V');
    assertEquals(p1.getWhere(), a1);
    assertEquals(p1.getOrientation(), 'V');
  }
  @Test
  public void test_hashCode() {
    Coordinate a1 = new Coordinate(1, 2);
    Coordinate a2 = new Coordinate(1, 2);
    Coordinate a3 = new Coordinate(2, 3);
    Placement p1 = new Placement(a1, 'v');
    Placement p2 = new Placement(a2, 'V');
    Placement p3 = new Placement(a3, 'V');
    assertEquals(p1.hashCode(), p2.hashCode());
    assertNotEquals(p2, p3);
  }
  @Test
  public void test_toString() {
    Coordinate a1 = new Coordinate(1, 2);
    Coordinate a2 = new Coordinate(1, 2);
    Coordinate a3 = new Coordinate(2, 3);
    Placement p1 = new Placement(a1, 'v');
    Placement p2 = new Placement(a2, 'V');
    Placement p3 = new Placement(a3, 'V');
    assertEquals(p1.toString(), p2.toString());
    assertNotEquals(p2.toString(), p3.toString());
  }


}
