package edu.duke.xh110.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class NoCollisionRuleCheckerTest {
  @Test
  public void test_onlyNoCollision() {
    PlacementRuleChecker<Character> placementRuleChecker = new NoCollisionRuleChecker<Character>(null);
    Board<Character> theBoard = new BattleShipBoard<>(10, 10, placementRuleChecker,  'X');
    AbstractShipFactory<Character> f = new V1ShipFactory();
    Ship<Character> ship1 = f.makeSubmarine(new Placement(new Coordinate(4,5), 'H'));
    Ship<Character> ship2 = f.makeSubmarine(new Placement(new Coordinate(5,5), 'H'));
    Ship<Character> ship3 = f.makeSubmarine(new Placement(new Coordinate(4,6), 'H'));
    assertEquals(null, theBoard.tryAddShip(ship1));
    assertEquals(null, theBoard.tryAddShip(ship2));
    assertEquals("That placement is invalid: the ship overlaps another ship.\n", theBoard.tryAddShip(ship3));
  }

  @Test
  public void test_bothRuleChecker() {
    //PlacementRuleChecker<Character> placementRuleChecker2 = new NoCollisionRuleChecker<Character>(null);
    //PlacementRuleChecker<Character> placementRuleChecker1 = new InBoundsRuleChecker<>(placementRuleChecker2);
    Board<Character> theBoard = new BattleShipBoard<Character>(10, 10, 'X');
    AbstractShipFactory<Character> f = new V1ShipFactory();
    Ship<Character> ship1 = f.makeSubmarine(new Placement(new Coordinate(4,5), 'H'));
    Ship<Character> ship2 = f.makeSubmarine(new Placement(new Coordinate(5,5), 'H'));
    Ship<Character> ship3 = f.makeSubmarine(new Placement(new Coordinate(4,6), 'H'));
    Ship<Character> ship4 = f.makeSubmarine(new Placement(new Coordinate(10,2), 'H'));
    assertEquals(null, theBoard.tryAddShip(ship1));
    assertEquals(null, theBoard.tryAddShip(ship2));
    assertEquals("That placement is invalid: the ship overlaps another ship.\n", theBoard.tryAddShip(ship3));
    assertEquals("That placement is invalid: the ship goes off the bottom of the board.\n", theBoard.tryAddShip(ship4));
  }

}
