package edu.duke.xh110.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class SonarScanTest {
  @Test
  public void test_makeScan() {
    Board<Character> board = new BattleShipBoard<Character>(10, 10, 'X');
    Ship<Character> s1 = new RectangleShip<Character>("Submarine", new Coordinate(1,2), 1, 1, 's', '*', 'V');
    Ship<Character> s2 = new RectangleShip<Character>("Carrier", new Coordinate(1,3), 1, 1, 's', '*', 'V');
    Ship<Character> s3 = new RectangleShip<Character>("Destroyer", new Coordinate(2,2), 1, 1, 's', '*', 'V');
    Ship<Character> s4 = new RectangleShip<Character>("Battleship", new Coordinate(2,3), 1, 1, 's', '*', 'V');
    board.tryAddShip(s1);
    board.tryAddShip(s2);
    board.tryAddShip(s3);
    board.tryAddShip(s4);
    String ans = "";
    ans += "Submarines occupy " + 1 + " squares\n";
    ans += "Destroyers occupy " + 1 + " squares\n";
    ans += "Battleships occupy " + 1 + " squares\n";
    ans += "Carriers occupy " + 1 + " squares\n";    
    String ans2 = SonarScan.makeScan(board, new Coordinate(1,2));
    assertEquals(ans, ans2);
  }

  @Test
  public void test_checkSonar() {
    Board<Character> board = new BattleShipBoard<Character>(10, 10, 'X');
    assertNotEquals(null, SonarScan.checkSonar(board, new Coordinate(20,1)));
    assertNotEquals(null, SonarScan.checkSonar(board, new Coordinate(1,20)));        

  }

  @Test
  public void test_tryScan() {
    Board<Character> board = new BattleShipBoard<Character>(10, 10, 'X');
    assertNotEquals(null, SonarScan.tryScan(board, new Coordinate(1,20), System.out));
  }

}
