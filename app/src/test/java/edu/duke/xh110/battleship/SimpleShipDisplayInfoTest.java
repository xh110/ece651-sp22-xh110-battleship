package edu.duke.xh110.battleship;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class SimpleShipDisplayInfoTest {
  @Test
  public void test_getInfo() {
    SimpleShipDisplayInfo<Character> dis = new SimpleShipDisplayInfo<Character>('s', '*');
    assertEquals('*', dis.getinfo(new Coordinate(0,0), true));
    assertEquals('s', dis.getinfo(new Coordinate(0,0), false));
  }

}

