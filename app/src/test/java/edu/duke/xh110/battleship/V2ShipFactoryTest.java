package edu.duke.xh110.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class V2ShipFactoryTest {
  @Test
  public void test_makeOldShip() {
    V2ShipFactory f = new V2ShipFactory();

    // make a submarine
    Placement v1_1 = new Placement(new Coordinate(1, 2), 'V');
    Ship<Character> dst1 = f.makeSubmarine(v1_1);
    checkShip(dst1, "Submarine", 's', new Coordinate(1, 2), new Coordinate(2, 2));

    // make a destroyer
    Placement v1_2 = new Placement(new Coordinate(1, 2), 'V');
    Ship<Character> dst2 = f.makeDestroyer(v1_2);
    checkShip(dst2, "Destroyer", 'd', new Coordinate(1, 2), new Coordinate(2, 2), new Coordinate(3, 2));


    // make a H submarine
    Placement v1_1_h  = new Placement(new Coordinate(1, 2), 'H');
    Ship<Character> dst1_h = f.makeSubmarine(v1_1_h);
    checkShip(dst1_h, "Submarine", 's', new Coordinate(1, 2), new Coordinate(1, 3));

    // check invalid orientation
    Placement v1_5 = new Placement(new Coordinate(1, 2), 'K');
    assertThrows(IllegalArgumentException.class, ()->f.makeSubmarine(v1_5));
  }

  private void checkShip(Ship<Character>testShip, String expectedName,
                         char expectedLetter, Coordinate... expectedLocs) {
    // check name
    assertEquals(expectedName, testShip.getName());    
    // check location: occupy
    for (int i = 0; i < expectedLocs.length; i++) {
      assertEquals(true, testShip.occupiesCoordinates(expectedLocs[i]));
    }
    // check letter
    assertEquals(expectedLetter, testShip.getDisplayInfoAt(expectedLocs[0], true));

  }

  @Test
  public void test_makeNewShip() {
    V2ShipFactory f = new V2ShipFactory();

    // make a BattleShip
    Placement p1 = new Placement(new Coordinate(3,4), 'u');
    Ship<Character> s1 = f.makeBattleship(p1);
    assertEquals(false, s1.occupiesCoordinates(new Coordinate(3,4)));
    assertEquals(true, s1.occupiesCoordinates(new Coordinate(3,5)));
    assertEquals(false, s1.occupiesCoordinates(new Coordinate(3,6)));
    assertEquals(true, s1.occupiesCoordinates(new Coordinate(4,4)));
    assertEquals(true, s1.occupiesCoordinates(new Coordinate(4,5)));
    assertEquals(true, s1.occupiesCoordinates(new Coordinate(4,6)));    


    // make a Carrier
    Placement p2 = new Placement(new Coordinate(3,4), 'd');
    Ship<Character> s2 = f.makeCarrier(p2);
    assertEquals(true, s2.occupiesCoordinates(new Coordinate(3,4)));
    assertEquals(true, s2.occupiesCoordinates(new Coordinate(4,4)));
    assertEquals(true, s2.occupiesCoordinates(new Coordinate(5,4)));
    assertEquals(false, s2.occupiesCoordinates(new Coordinate(6,4)));
    assertEquals(false, s2.occupiesCoordinates(new Coordinate(7,4)));
    assertEquals(false, s2.occupiesCoordinates(new Coordinate(3,5)));
    assertEquals(true, s2.occupiesCoordinates(new Coordinate(4,5)));
    assertEquals(true, s2.occupiesCoordinates(new Coordinate(5,5)));
    assertEquals(true, s2.occupiesCoordinates(new Coordinate(6,5)));
    assertEquals(true, s2.occupiesCoordinates(new Coordinate(7,5)));
    



    // check invalid orientation
    Placement v1_5 = new Placement(new Coordinate(1, 2), 'K');
    assertThrows(IllegalArgumentException.class, ()->f.makeBattleship(v1_5));
  }
  




}
