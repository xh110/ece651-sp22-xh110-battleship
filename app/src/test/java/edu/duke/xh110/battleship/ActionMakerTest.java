package edu.duke.xh110.battleship;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.StringReader;

import com.google.common.primitives.Bytes;

import org.junit.jupiter.api.Test;

public class ActionMakerTest {
  @Test
  public void test_performScan() throws IOException {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer player1 = createTextPlayer(10, 20, "B2\nC8H\na4v\n", bytes);
    TextPlayer player2 = createTextPlayer(10, 20, "", bytes);
    ActionMaker maker = new ActionMaker(player1.getInput(),player1.getOut(), 3, 3);
    Board<Character> board1 = player1.getBoard();
    Board<Character> board2 = player2.getBoard();
    Placement p1 = new Placement(new Coordinate(1,2), 'U');
    Placement p2 = new Placement(new Coordinate(1,2), 'U');    
    Ship<Character> s1 = new TShapedShip<>(p1, "Battleship", 'b', '*');
    Ship<Character> s2 = new TShapedShip<>(p2, "Battleship", 'b', '*');
    board2.tryAddShip(s2);
    maker.performScan(player2);
    String expect = "enter the Coordinate you want to scan\n" +
      "Submarines occupy 0 squares\n" +
      "Destroyers occupy 0 squares\n" +
      "Battleships occupy 4 squares\n" +
      "Carriers occupy 0 squares\n";

    assertEquals(expect, bytes.toString());
  }

  @Test
  public void test_performFire() throws IOException{
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer player1 = createTextPlayer(10, 20, "B2\nB3\nz7\n", bytes);
    TextPlayer player2 = createTextPlayer(10, 20, "", bytes);
    ActionMaker maker = new ActionMaker(player1.getInput(),player1.getOut(), 3, 3);
    Board<Character> board1 = player1.getBoard();
    Board<Character> board2 = player2.getBoard();
    Placement p1 = new Placement(new Coordinate(1,2), 'U');
    Placement p2 = new Placement(new Coordinate(1,2), 'U');    
    Ship<Character> s1 = new TShapedShip<>(p1, "Battleship", 'b', '*');
    Ship<Character> s2 = new TShapedShip<>(p2, "Battleship", 'b', '*');
    board2.tryAddShip(s2);
    maker.performFire(player2);
    String expect = "enter the Coordinate you want to fire\n" +
      "You missed!\n";
    assertEquals(expect, bytes.toString());
    
    bytes.reset();
    maker.performFire(player2);
    expect = "enter the Coordinate you want to fire\n" +
      "You hit a Battleship!\n";
    assertEquals(expect, bytes.toString());
    
    bytes.reset();
    maker.performFire(player2);
    expect = "enter the Coordinate you want to fire\n";
    assertEquals(expect, bytes.toString());

  }

  @Test
  public void test_performMove() throws IOException{
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer player1 = createTextPlayer(10, 20, "B3\nF6\na4v\n", bytes);
    TextPlayer player2 = createTextPlayer(10, 20, "", bytes);
    ActionMaker maker = new ActionMaker(player1.getInput(),player1.getOut(), 3, 3);
    Board<Character> board1 = player1.getBoard();
    Board<Character> board2 = player2.getBoard();
    Placement p1 = new Placement(new Coordinate(1,2), 'U');
    Placement p2 = new Placement(new Coordinate(1,2), 'U');    
    Ship<Character> s1 = new TShapedShip<>(p1, "Battleship", 'b', '*');
    Ship<Character> s2 = new TShapedShip<>(p2, "Battleship", 'b', '*');
    board1.tryAddShip(s1);
    maker.performMove(player2);
    String expect = "enter the Coordinate you want to pick a ship\n" +
      "enter the Placement you want to move the ship\n";
    assertEquals(expect, bytes.toString());
  }

  @Test
  public void test_playOneTurn() throws IOException{
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer player1 = createTextPlayer(10, 20, "M\nB3\nF6L\na4v\n", bytes);
    TextPlayer player2 = createTextPlayer(10, 20, "", bytes);
    ActionMaker maker = new ActionMaker(player1.getInput(),player1.getOut(), 3, 3);
    Board<Character> board1 = player1.getBoard();
    Board<Character> board2 = player2.getBoard();
    Placement p1 = new Placement(new Coordinate(1,2), 'U');
    Placement p2 = new Placement(new Coordinate(1,2), 'U');    
    Ship<Character> s1 = new TShapedShip<>(p1, "Battleship", 'b', '*');
    Ship<Character> s2 = new TShapedShip<>(p2, "Battleship", 'b', '*');
    board1.tryAddShip(s1);
    maker.playOneTurn(player1, player2);
    String expect = "enter the Coordinate you want to pick a ship\n" +
      "enter the Placement you want to move the ship\n";
    //assertEquals("", bytes.toString());
    bytes.reset();
    player1.printTwoBoard(player2);
    //assertEquals("", bytes.toString());
  }

  
  private TextPlayer createTextPlayer(int w, int h, String inputData, OutputStream bytes) {
    BufferedReader input = new BufferedReader(new StringReader(inputData));
    PrintStream output = new PrintStream(bytes, true);
    Board<Character> board = new BattleShipBoard<Character>(w, h, 'X');
    V1ShipFactory shipFactory = new V1ShipFactory();
    return new TextPlayer("A", board, input, output, shipFactory);
  }

  @Test
  public void EOF_action() {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer player11 = createTextPlayer(10, 20, "", bytes);
    TextPlayer player12 = createTextPlayer(10, 20, "", bytes);
    ActionMaker maker = new ActionMaker(player11.getInput(),player12.getOut(), 3, 3);
    assertThrows(EOFException.class, ()->maker.playOneTurn(player11, player12));

    ByteArrayOutputStream bytes2 = new ByteArrayOutputStream();
    TextPlayer player21 = createTextPlayer(10, 20, "M", bytes2);
    TextPlayer player22 = createTextPlayer(10, 20, "", bytes2);
    ActionMaker maker2 = new ActionMaker(player21.getInput(),player22.getOut(), 3, 3);
    assertThrows(EOFException.class, ()->maker2.playOneTurn(player21, player22));

    ByteArrayOutputStream bytes3 = new ByteArrayOutputStream();
    TextPlayer player31 = createTextPlayer(10, 20, "S", bytes3);
    TextPlayer player32 = createTextPlayer(10, 20, "", bytes3);
    ActionMaker maker3 = new ActionMaker(player31.getInput(),player32.getOut(), 3, 3);
    assertThrows(EOFException.class, ()->maker3.playOneTurn(player31, player32));
    
    
  }
  

}
