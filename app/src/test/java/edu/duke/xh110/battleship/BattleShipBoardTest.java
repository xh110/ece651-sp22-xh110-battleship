package edu.duke.xh110.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class BattleShipBoardTest {
  @Test
  public void test_width_and_height() {
    Board<Character> b1 = new BattleShipBoard<Character>(10, 20, 'X');
    assertEquals(10, b1.getWidth());
    assertEquals(20, b1.getHeight());
  }

  @Test
  public void test_invalid_dimensions() {
    assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(10, 0, 'X'));
    assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(0, 20, 'X'));
    assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(10, -5, 'X'));
    assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(-8, 20, 'X'));
  }

  @Test
  public void test_add_ship() {
    BattleShipBoard<Character> b = new BattleShipBoard<Character>(2, 2, 'X');
    Character expect[][] = new Character[2][2];
    for (int i = 0; i < expect.length; i++) {
      for (int j = 0; j < expect.length; j++) {
        expect[i][j] = null;
      }
    }
    checkWhatIsAtBoard(b, expect);

    for (int i = 0; i < expect.length; i++) {
      for (int j = 0; j < expect[i].length; j++) {
        Coordinate where = new Coordinate(i, j);
        Ship<Character> s= new RectangleShip<Character>(where, 's', '*');
        b.tryAddShip(s);
      }
    }
    for (int i = 0; i < expect.length; i++) {
      for (int j = 0; j < expect[i].length; j++) {
        expect[i][j] = 's';
      }
    }
    checkWhatIsAtBoard(b, expect);

  }

  @Test
  public void test_fireAt() {
    BattleShipBoard<Character> b = new BattleShipBoard<Character>(3, 1, 'X');
    Ship<Character> exp1 = new RectangleShip<Character>(new Coordinate(0,0), 's', '*');
    b.tryAddShip(exp1);
    assertSame(exp1, b.fireAt(new Coordinate(0, 0)));
    assertEquals(true, exp1.isSunk());
    assertEquals(null, b.whatIsAtForEnemy(new Coordinate(0, 1)));
    b.fireAt(new Coordinate(0, 1));
    assertEquals('X', b.whatIsAtForEnemy(new Coordinate(0, 1)));
    


  }

  private <T> void checkWhatIsAtBoard(BattleShipBoard<T> b, T[][] expect) {
    for (int i = 0; i < expect.length; i++) {
      for (int j = 0; j < expect[i].length; j++) {       
        assertEquals(b.whatIsAtForSelf(new Coordinate(i, j)), expect[i][j]);
      }
    }
  }

  @Test
  public void test_isLose() {
    BattleShipBoard<Character> b = new BattleShipBoard<Character>(3, 1, 'X');
    Ship<Character> exp1 = new RectangleShip<Character>(new Coordinate(0,0), 's', '*');
    b.tryAddShip(exp1);
    assertEquals(false, b.isLose());
    b.fireAt(new Coordinate(0, 1));
    assertEquals(false, b.isLose());
    b.fireAt(new Coordinate(0, 0));
    assertEquals(true, b.isLose());  
  }
}
