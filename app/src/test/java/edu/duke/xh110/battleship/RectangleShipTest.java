package edu.duke.xh110.battleship;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.HashSet;

import org.junit.jupiter.api.Test;

public class RectangleShipTest {
  @Test
  public void test_makeCoords() {
    HashSet<Coordinate> set = RectangleShip.makeCoords(new Coordinate(0,0), 2, 1);
    HashSet<Coordinate> expected = new HashSet<Coordinate>();
    expected.add(new Coordinate(0,0));
    expected.add(new Coordinate(0,1));
    assertEquals(expected.contains(new Coordinate(0,0)), set.contains(new Coordinate(0, 0)));
    assertEquals(expected.contains(new Coordinate(0,1)), set.contains(new Coordinate(0, 1)));
  }

  @Test
  public void test_Constructor() {
    RectangleShip<Character> myShip = new RectangleShip<Character>(new Coordinate(0, 0), 's', '*');
    assertEquals(true, myShip.occupiesCoordinates(new Coordinate(0, 0)));
    assertEquals(false, myShip.occupiesCoordinates(new Coordinate(0, 1)));
    assertEquals(false, myShip.occupiesCoordinates(new Coordinate(0, 2)));
  }

  @Test
  public void test_recordhit_washit() {
    RectangleShip<Character> myShip = new RectangleShip<Character>("testship", new Coordinate(4, 5), 2, 2, 's', '*', 'V');
    myShip.recordHitAt(new Coordinate(4, 5));
    myShip.recordHitAt(new Coordinate(5, 6));
    assertEquals(true, myShip.wasHitAt(new Coordinate(4, 5)));
    assertEquals(true, myShip.wasHitAt(new Coordinate(5, 6)));
    assertEquals(false, myShip.wasHitAt(new Coordinate(5, 5)));

    assertThrows(IllegalArgumentException.class, ()->myShip.recordHitAt(new Coordinate(7, 7)));
    assertThrows(IllegalArgumentException.class, ()->myShip.wasHitAt(new Coordinate(7, 7)));
  }

  @Test
  public void test_isSunk() {
    RectangleShip<Character> myShip = new RectangleShip<Character>("testship", new Coordinate(4, 5), 2, 2, 's', '*', 'V');
    myShip.recordHitAt(new Coordinate(4, 5));
    myShip.recordHitAt(new Coordinate(5, 6));
    assertEquals(false, myShip.isSunk());
    myShip.recordHitAt(new Coordinate(4, 6));
    myShip.recordHitAt(new Coordinate(5, 5));
    assertEquals(true, myShip.isSunk());
  }

  @Test
  public void test_getDisolayInfoAt() {
    RectangleShip<Character> myShip = new RectangleShip<Character>("testship", new Coordinate(4, 5), 2, 2, 's', '*', 'V');
    myShip.recordHitAt(new Coordinate(4, 5));
    myShip.recordHitAt(new Coordinate(5, 6));
    assertEquals('*', myShip.getDisplayInfoAt(new Coordinate(4, 5), true));
    assertEquals('*', myShip.getDisplayInfoAt(new Coordinate(5, 6), true));
    assertEquals('s', myShip.getDisplayInfoAt(new Coordinate(4, 6), true));
    assertEquals('s', myShip.getDisplayInfoAt(new Coordinate(5, 5), true));
    assertEquals('s', myShip.getDisplayInfoAt(new Coordinate(4, 5), false));
    assertEquals(null, myShip.getDisplayInfoAt(new Coordinate(5, 5), false));
  }

  @Test
  public void test_getName() {
    RectangleShip<Character> myShip = new RectangleShip<Character>("testship", new Coordinate(4, 5), 2, 2, 's', '*', 'V');
    assertEquals("testship", myShip.getName());
  }

  @Test
  public void test_getCoordinate() {
    RectangleShip<Character> myShip = new RectangleShip<Character>("testship", new Coordinate(4, 5), 1, 2, 's', '*', 'V');
    Iterable<Coordinate> set = myShip.getCoordiantes();
    boolean check = false;
    for (Coordinate where: set) {
      check = check | where.equals(new Coordinate(4,5));
    }
    assertEquals(true, check);

    check = false;
    for (Coordinate where: set) {
      check = check | where.equals(new Coordinate(5,5));
    }
    assertEquals(true, check);

    check = false;
    for (Coordinate where: set) {
      check = check | where.equals(new Coordinate(5,6));
    }
    assertEquals(false, check);

  }
}
