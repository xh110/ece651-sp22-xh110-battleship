package edu.duke.xh110.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class InBoundsRuleCheckerTest {
  @Test
  public void test_myRuleChecker() {
    Board<Character> theBoard = new BattleShipBoard<>(5, 5, 'X');
    AbstractShipFactory<Character> f = new V1ShipFactory();
    Ship<Character> ship1 = f.makeSubmarine(new Placement(new Coordinate(-1, 3), 'H'));
    Ship<Character> ship2 = f.makeSubmarine(new Placement(new Coordinate(9, 2), 'H'));
    Ship<Character> ship3 = f.makeSubmarine(new Placement(new Coordinate(1, -1), 'H'));
    Ship<Character> ship4 = f.makeSubmarine(new Placement(new Coordinate(1, 9), 'H'));
    Ship<Character> ship5 = f.makeSubmarine(new Placement(new Coordinate(2, 2), 'H'));
    assertEquals("That placement is invalid: the ship goes off the top of the board.\n", theBoard.tryAddShip(ship1));
    assertEquals("That placement is invalid: the ship goes off the bottom of the board.\n", theBoard.tryAddShip(ship2));
    assertEquals("That placement is invalid: the ship goes off the left of the board.\n", theBoard.tryAddShip(ship3));
    assertEquals("That placement is invalid: the ship goes off the right of the board.\n", theBoard.tryAddShip(ship4));
    assertEquals(null, theBoard.tryAddShip(ship5));
  }


}
