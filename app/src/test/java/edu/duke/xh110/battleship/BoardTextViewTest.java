package edu.duke.xh110.battleship;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

public class BoardTextViewTest {
  @Test
  public void test_display__2by2() {
    Board<Character> b1 = new BattleShipBoard<Character>(2, 2, 'X');
    BoardTextView view = new BoardTextView(b1);
    String expectedHeader = "  0|1\n";
    String expectedBody =  "A  |  A\n" +   "B  |  B\n";
    emptyBoardHelper(2,2,expectedHeader,expectedBody);

    b1.tryAddShip(new RectangleShip<Character>(new Coordinate(0,0), 's', '*'));
    view = new BoardTextView(b1);
    expectedHeader = "  0|1\n";
    expectedBody =  "A s|  A\n" +   "B  |  B\n";
    String expected = expectedHeader + expectedBody + expectedHeader;
    assertEquals(expected, view.displayMyOwnBoard());

    b1.fireAt(new Coordinate(0,0));
    b1.fireAt(new Coordinate(0,1));
    expectedHeader = "  0|1\n";
    expectedBody =  "A s|X A\n" +   "B  |  B\n";
    expected = expectedHeader + expectedBody + expectedHeader;
    assertEquals(expected, view.displayEnemyBoard());

  }
  @Test
  public void test_displayBothBoard() {
    Board<Character> b1 = new BattleShipBoard<Character>(4, 2, 'X');
    Board<Character> b2 = new BattleShipBoard<Character>(4, 2, 'X');
    b1.tryAddShip(new RectangleShip<Character>(new Coordinate(0,0), 's', '*'));
    b1.tryAddShip(new RectangleShip<Character>(new Coordinate(0,1), 's', '*'));
    b1.tryAddShip(new RectangleShip<Character>(new Coordinate(0,2), 's', '*'));
    b2.tryAddShip(new RectangleShip<Character>(new Coordinate(0,0), 's', '*'));
    b2.tryAddShip(new RectangleShip<Character>(new Coordinate(0,1), 's', '*'));
    b2.tryAddShip(new RectangleShip<Character>(new Coordinate(0,2), 's', '*'));
    BoardTextView view1 = new BoardTextView(b1);
    BoardTextView view2 = new BoardTextView(b2);
    b1.fireAt(new Coordinate(0,0));
    b1.fireAt(new Coordinate(0,1));
    b1.fireAt(new Coordinate(1,0));
    b1.fireAt(new Coordinate(1,1));
    b2.fireAt(new Coordinate(0,0));
    b2.fireAt(new Coordinate(0,1));
    b2.fireAt(new Coordinate(1,0));
    b2.fireAt(new Coordinate(1,1));
    String header1 = "oce1";
    String header2 = "oce2";
    String space = "                ";
    String ans = view1.displayMyBoardWithEnemyNextToIt(view2, header1, header2);
    String expected =
      "     " + header1 +  space + "     " + header2 + "\n" +
      "  0|1|2|3  " + space + "  0|1|2|3\n" +
      "A *|*|s|  A" + space + "A s|s| |  A\n" +
      "B  | | |  B" + space + "B X|X| |  B\n" +
      "  0|1|2|3  " + space + "  0|1|2|3\n";
    assertEquals("\n" + expected, "\n" + ans);

    header1 = "My ocean";
    header2 = "oce2";
    space = "                ";
    ans = view1.displayMyBoardWithEnemyNextToIt(view2, header1, header2);
    expected =
      "     " + header1 +  space + "   " + header2 + "\n" +
      "  0|1|2|3  " + space + "  0|1|2|3\n" +
      "A *|*|s|  A" + space + "A s|s| |  A\n" +
      "B  | | |  B" + space + "B X|X| |  B\n" +
      "  0|1|2|3  " + space + "  0|1|2|3\n";
    assertEquals("\n" + expected, "\n" + ans);

  }
  
  private void emptyBoardHelper(int w, int h, String expectedHeader, String expectedBody) {
    Board<Character> b1 = new BattleShipBoard<Character>(w,h, 'X');
    BoardTextView view = new BoardTextView(b1);
    assertEquals(expectedHeader, view.makeHeader());
    String expected = expectedHeader + expectedBody + expectedHeader;
    assertEquals(expected, view.displayMyOwnBoard());
   
  }
  @Test
  public void test_invalid_board_size() {
    Board<Character> wideBoard = new BattleShipBoard<Character>(11, 20, 'X');
    Board<Character> tallBoard = new BattleShipBoard<Character>(10,27, 'X');
    assertThrows(IllegalArgumentException.class, ()->new BoardTextView(wideBoard));
    assertThrows(IllegalArgumentException.class, ()->new BoardTextView(tallBoard));
  }

}
